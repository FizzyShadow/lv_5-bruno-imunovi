﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV_5_Bruno_Šimunović
{
    class ShippingService
    {
        private double weightPrice;
        public ShippingService (double weightPrice)
        {
            this.weightPrice = weightPrice;
        }
        public double WeightPrice { get { return weightPrice; } }
        public double ShippingPrice(IShipable shipable)
        {
            return shipable.Weight * WeightPrice;
        }

    }
}
