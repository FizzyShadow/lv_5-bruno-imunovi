﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV_5_Bruno_Šimunović
{
    interface IShipable
    {
        double Price { get; }
        double Weight { get; }
        string Description(int depth = 0);
    }
}
