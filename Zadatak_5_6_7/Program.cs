﻿using System;

namespace Zadatak_5_6_7
{
    class Program
    {
        static void Main(string[] args)
        {
            string biljeska = "Bilo jednom davno";
            GreenThema greenThema = new GreenThema();
            LightTheme lightTheme = new LightTheme();
            ReminderNote reminderNote = new ReminderNote(biljeska, greenThema);
            GroupNote groupNote = new GroupNote(biljeska, lightTheme);
            groupNote.Dodaj("Pero");
            groupNote.Dodaj("Maki");
            groupNote.Dodaj("Jurica");
            groupNote.Dodaj("Miki");
            reminderNote.Show();
            groupNote.Show();
            groupNote.Obrisi("Jurica");
            groupNote.Show();

            Notebook notebook = new Notebook();
            notebook.AddNote(reminderNote);
            notebook.AddNote(groupNote);
            notebook.ChangeTheme(greenThema);
            notebook.Display();

            Notebook2 notebook2 = new Notebook2(lightTheme);
            notebook2.AddNote(reminderNote);
            notebook2.AddNote(groupNote);
            notebook2.Display();


        }
    }
}
