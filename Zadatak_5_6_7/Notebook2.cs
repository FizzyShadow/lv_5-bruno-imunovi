﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zadatak_5_6_7
{
    class Notebook2
    {
        private List<Note> notes;
        private ITheme theme;
        public Notebook2(ITheme theme)
        { 
            this.notes = new List<Note>();
            this.theme = theme;
        }
        public void AddNote(Note note1) 
        { 
            this.notes.Add(note1);
            foreach (Note note in this.notes)
            {
                note.Theme = theme;
            }
        }
        public void ChangeTheme(ITheme theme)
        {
            foreach (Note note in this.notes)
            {
                note.Theme = theme;
            }
        }
        public void Display()
        {
            foreach (Note note in this.notes)
            {
                note.Show();
                Console.WriteLine("\n");
            }
        }
    }
}
