﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zadatak_5_6_7
{
    class GroupNote :  Note
    {
        private List<string> notes;
        public GroupNote(string message, ITheme theme) : base(message, theme) 
        {
            notes = new List<string>();
        }
        public void Dodaj(string name)
        {
            notes.Add(name);
        }

        public void Obrisi(string name)
        {
            notes.Remove(name);
        }
        public override void Show()
        {
            this.ChangeColor();
            Console.WriteLine("GROUP OF: ");
            foreach (string ime in notes)
            {
                Console.WriteLine(ime + " ");
            }
            Console.ResetColor();
            
        }
    }
}
