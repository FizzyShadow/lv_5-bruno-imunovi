﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zadatak_3_4
{
    class ConsoleLogger
    {
        private static ConsoleLogger instance;
        private ConsoleLogger() { }
        public static ConsoleLogger GetInstance()
        {
            if (instance == null)
            {
                instance = new ConsoleLogger();
            }
            return instance;
        }
        public void Log(string data)
        {
            Console.WriteLine("Pristupljeno u: "+ DateTime.Now);
            Console.WriteLine("Zabilježeno!");
            Console.WriteLine(data);

        }
    }
}
