﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;

namespace Zadatak_3_4
{
    interface IDataset
    {
        ReadOnlyCollection<List<string>> GetData();
    }
}
