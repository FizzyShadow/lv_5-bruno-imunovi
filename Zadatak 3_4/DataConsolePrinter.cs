﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;

namespace Zadatak_3_4
{
    class DataConsolePrinter
    {
        public void Print(IDataset dataset)
        {
            
            if (dataset.GetData() == null)
            {
                Console.WriteLine("Nemate pravo pristupa listi.");
            }
            else
            {
                foreach (List<string> lista1 in dataset.GetData())
                {
                    foreach (string lista2 in lista1)
                    {
                        Console.Write(lista2 + " ");
                    }
                    Console.WriteLine();
                }
            }
            
        }
    }
}
