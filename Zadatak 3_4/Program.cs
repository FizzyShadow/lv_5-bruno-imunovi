﻿using System;

namespace Zadatak_3_4
{
    class Program
    {
        static void Main(string[] args)
        {
            string path = "text.csv";
            Dataset dataset = new Dataset(path);
            ProtectionProxyDataset protectionProxy = new ProtectionProxyDataset(User.GenerateUser("Pero"));
            ProtectionProxyDataset protectionProxy1 = new ProtectionProxyDataset(User.GenerateUser("Mirko"));
            ProtectionProxyDataset protectionProxy2 = new ProtectionProxyDataset(User.GenerateUser("Stjepan"));
            ProtectionProxyDataset protectionProxy3 = new ProtectionProxyDataset(User.GenerateUser("Stanislav"));
            ProtectionProxyDataset protectionProxy4 = new ProtectionProxyDataset(User.GenerateUser("Hera"));
            VirtualProxyDataset virtualProxyDataset = new VirtualProxyDataset(path);
            DataConsolePrinter dataConsolePrinter = new DataConsolePrinter();
            dataConsolePrinter.Print(protectionProxy);
            dataConsolePrinter.Print(protectionProxy1);
            dataConsolePrinter.Print(protectionProxy2);
            dataConsolePrinter.Print(protectionProxy3);
            dataConsolePrinter.Print(protectionProxy4);
            dataConsolePrinter.Print(virtualProxyDataset);
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();

            ProxyLogger proxyLogger = new ProxyLogger(path);
            proxyLogger.GetData();
            
        }
    }
}
