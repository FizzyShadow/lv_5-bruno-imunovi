﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;

namespace Zadatak_3_4
{
    class ProxyLogger : IDataset
    {
        private string filePath;
        private Dataset dataset;
        public ProxyLogger(string filePath)
        {
            this.filePath = filePath;
        }
        public ReadOnlyCollection<List<string>> GetData()
        {
            
            if (dataset == null)
            {
                dataset = new Dataset(filePath);
            }
            
            StringBuilder stringBuilder = new StringBuilder();
            foreach (List<string> lista1 in dataset.GetData())
            {
                foreach (string lista2 in lista1)
                {
                    stringBuilder.Append(lista2 + " ");
                }

            }
                
            ConsoleLogger.GetInstance().Log(stringBuilder.ToString());
            return dataset.GetData();

        }

    }
}
